import { isRSAA, apiMiddleware } from 'redux-api-middleware'

import {
  REFRESH_TOKEN_SUCCESS,
  refreshAccessToken
} from './src/auth/actions/RefreshToken'
import { getRefreshToken, isAccessTokenExpired } from './src/auth/utils/authUtils'

export function createApiMiddleware () {
  const postponedRSAAs = []

  return ({ dispatch, getState }) => {
    const rsaaMiddleware = apiMiddleware({ dispatch, getState })
    return next => (action) => {
      const nextCheckPostponed = nextAction => {
        // Run postponed actions after token refresh
        if (nextAction.type === REFRESH_TOKEN_SUCCESS) {
          console.debug('nextCheckPostponed')
          console.debug(nextAction.type)
          next(nextAction)
          postponedRSAAs.forEach(postponed => {
            rsaaMiddleware(next)(postponed)
          })
        } else {
          next(nextAction)
        }
      }
      if (isRSAA(action)) {
        console.debug('Is RSAA -> True')
        const refreshToken = getRefreshToken(getState())

        if (refreshToken && isAccessTokenExpired(getState())) {
          console.debug('Access token is expired but we have refresh token')
          postponedRSAAs.push(action)
          console.debug('postponed RSAAs: ', postponedRSAAs)
          if (postponedRSAAs.length > 0) {
            return rsaaMiddleware(nextCheckPostponed)(refreshAccessToken(getState()))
          } else {
            return
          }
        }

        return rsaaMiddleware(next)(action)
      }
      return next(action)
    }
  }
}

export default createApiMiddleware
