import React from "react";
import { Platform } from "react-native";
import AndroidApp from "./AppNavigator.android";
import IosApp from "./AppNavigator.ios";

export default () => {
  if (Platform.OS === "android") return <AndroidApp />;
  return <IosApp />;
};
