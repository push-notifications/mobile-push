import React from "react";
import { createStackNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";
import HeaderButtons, {
  HeaderButton,
  Item
} from "react-navigation-header-buttons";
import RulesPage from "../../settings/rules/pages/RulesPage";
import CreateRuleContainer from "../../settings/rules/containers/CreateRuleContainer";
import EditRulePageContainer from "../../settings/rules/containers/EditRulePageContainer";

const IoniconsHeaderButton = passMeFurther => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={Icon}
    iconSize={25}
    color="white"
  />
);

export default createStackNavigator({
  RulesPage: {
    screen: RulesPage,
    navigationOptions: () => ({
      title: "Rules List",
      headerTitleStyle: {
        color: "white"
      },
      headerStyle: {
        backgroundColor: "#2196F3"
      },
      headerTintColor: "white"
    })
  },
  EditRulePage: {
    screen: EditRulePageContainer,
    navigationOptions: ({ navigation }) => {
      const { params = {} } = navigation.state;
      return {
        title: "Edit Rule",
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#2196F3"
        },
        headerTintColor: "white",
        headerRight: (
          <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
            <Item
              title="delete"
              iconName="md-trash"
              onPress={() => params.onClickDelete()}
            />
            <Item
              title="save"
              iconName="md-save"
              onPress={() => params.onClickSave()}
            />
          </HeaderButtons>
        )
      };
    }
  },
  CreateRulePage: {
    screen: CreateRuleContainer,
    navigationOptions: ({ navigation }) => {
      const { params = {} } = navigation.state;
      return {
        title: "New Rule",
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#2196F3"
        },
        headerTintColor: "white",
        headerRight: (
          <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
            <Item
              title="save"
              iconName="md-save"
              onPress={() => params.onClickSave()}
            />
          </HeaderButtons>
        )
      };
    }
  }
});
