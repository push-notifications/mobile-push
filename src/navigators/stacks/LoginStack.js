import { createStackNavigator } from "react-navigation";
import LoginPageWebViewContainer from "../../auth/pages/loginPage/LoginPageWebView/LoginPageWebViewContainer";
import LoginPage from "../../auth/pages/loginPage/LoginPage";

export default createStackNavigator(
  {
    LoginPage: {
      screen: LoginPage,
      navigationOptions: () => {
        return {
          headerTitleStyle: {
            color: "white"
          },
          title: `CERN Notification Center`,
          headerStyle: {
            backgroundColor: "#2196F3"
          }
        };
      }
    },
    LoginPageWebView: {
      screen: LoginPageWebViewContainer,
      navigationOptions: () => {
        return {
          headerTitleStyle: {
            color: "white"
          },
          headerStyle: {
            backgroundColor: "#2196F3"
          },
          headerTintColor: "white"
        };
      }
    }
  },
  {}
);
