import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as FindAllServicesActionCreators from "../../actions/FindAllServices";
import ServicesList from "../../components/ServicesList";

const mapStatetoProps = state => {
  return {
    services: state.services.servicesList.services
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(FindAllServicesActionCreators, dispatch);
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(ServicesList);
