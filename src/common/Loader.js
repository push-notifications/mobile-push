import React from "react";
import { StyleSheet, View, Modal, Text, ActivityIndicator } from "react-native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "#00000040"
  },
  activityIndicatorWrapper: {
    backgroundColor: "#FFFFFF",
    height: 100,
    width: 100,
    borderRadius: 5,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around"
  },
  text: {
    fontSize: 18,
    fontWeight: "bold"
  }
});

const Loader = props => {
  const { loading } = props;

  return (
    <Modal transparent animationType="none" visible={loading}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <Text style={styles.text}>Loading...</Text>
          <ActivityIndicator size="large" animating={loading} />
        </View>
      </View>
    </Modal>
  );
};

Loader.propTypes = {
  loading: PropTypes.bool.isRequired
};

export default Loader;
