import {
  GET_RULES,
  GET_RULES_SUCCESS,
  GET_RULES_FAILURE
} from "../actions/FindAllRules";
import { CREATE_RULE_SUCCESS } from "../actions/CreateRule";
import { DELETE_RULE_SUCCESS } from "../actions/DeleteRule";
import { UPDATE_RULE_SUCCESS } from "../actions/UpdateRule";

const INITIAL_STATE = {
  rules: [],
  error: null,
  loading: false
};

function processGetRules(state) {
  return {
    ...state,
    loading: true
  };
}

function processGetRulesSuccess(state, rules) {
  return {
    ...state,
    rules,
    loading: false
  };
}

function processGetRulesFailure(state, error) {
  return {
    ...state,
    rules: [],
    error,
    loading: false
  };
}

function processCreateRuleSuccess(state, rule) {
  return {
    ...state,
    rules: [...state.rules, rule]
  };
}

function processDeleteRuleSuccess(state, rule) {
  return {
    ...state,
    rules: state.rules.filter(r => r.id !== rule.id)
  };
}

function processUpdateRuleSuccess(state, rule) {
  return {
    ...state,
    rules: state.rules.map(r => (r.id === rule.id ? rule : r))
  };
}

export default function(state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case GET_RULES:
      return processGetRules(state);
    case GET_RULES_SUCCESS:
      return processGetRulesSuccess(state, action.payload);
    case GET_RULES_FAILURE:
      error = action.payload || { message: action.payload.message };
      return processGetRulesFailure(state, error);

    case CREATE_RULE_SUCCESS:
      return processCreateRuleSuccess(state, action.payload);
    case DELETE_RULE_SUCCESS:
      return processDeleteRuleSuccess(state, action.payload);
    case UPDATE_RULE_SUCCESS:
      return processUpdateRuleSuccess(state, action.payload);

    default:
      return state;
  }
}
