import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as updateRuleActionsCreators from "../actions/UpdateRule";
import * as deleteRuleActionCreators from "../actions/DeleteRule";
import EditRulePage from "../pages/EditRulePage";

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(updateRuleActionsCreators, dispatch),
    ...bindActionCreators(deleteRuleActionCreators, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditRulePage);
