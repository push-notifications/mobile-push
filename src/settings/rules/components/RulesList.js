import React, { useEffect } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";

import Separator from "../../../common/Separator";

const styles = StyleSheet.create({
  rule: {
    margin: 10,
    fontSize: 16
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});

const RulesList = props => {
  const { rules, navigation, findAllRules } = props;

  useEffect(() => {
    findAllRules();
  });

  if (rules.length === 0)
    return (
      <View style={styles.center}>
        <Text>
          Add a rule to decide how you want to receive your notifications.
        </Text>
      </View>
    );

  return (
    <FlatList
      data={rules.map((n, i) => ({ key: i.toString(), rule: n }))}
      renderItem={({ item }) => (
        <Text
          style={styles.rule}
          onPress={() =>
            navigation.navigate("EditRulePage", { rule: item.rule })
          }
        >
          {item.rule.name}
        </Text>
      )}
      ItemSeparatorComponent={Separator}
    />
  );
};

RulesList.propTypes = {
  rules: PropTypes.arrayOf(Object).isRequired,
  findAllRules: PropTypes.func.isRequired
};

export default RulesList;
