import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../../auth/utils/authUtils";

// Create new notification
export const UPDATE_RULE = "UPDATE_RULE";
export const UPDATE_RULE_SUCCESS = "UPDATE_RULE_SUCCESS";
export const UPDATE_RULE_FAILURE = "UPDATE_RULE_FAILURE";

export const updateRule = rule => {
  return {
    [RSAA]: {
      endpoint: `${BASE_URL}/rules`,
      method: "PUT",
      body: JSON.stringify({ rule }),
      credentials: "include",
      headers: withAuth({ "Content-Type": "application/json" }),
      types: [UPDATE_RULE, UPDATE_RULE_SUCCESS, UPDATE_RULE_FAILURE]
    }
  };
};
