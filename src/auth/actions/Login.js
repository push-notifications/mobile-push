import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";

export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const login = code => ({
  [RSAA]: {
    endpoint: `${BASE_URL}/login`,
    method: "POST",
    body: JSON.stringify({ code }),
    credentials: "include",
    headers: { "Content-Type": "application/json" },
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE]
  }
});
