export const UPDATE_FILTER_NOTIFCATIONS_FUNCTION =
  "UPDATE_FILTER_NOTIFCATIONS_FUNCTION";

export const updateFilterNotificationsFunction = filterFunction => ({
  type: UPDATE_FILTER_NOTIFCATIONS_FUNCTION,
  payload: filterFunction
});
