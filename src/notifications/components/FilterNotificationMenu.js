import React, { Component } from "react";
import { Alert } from "react-native";
import HeaderButtons, {
  HeaderButton,
  Item
} from "react-navigation-header-buttons";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

const IoniconsHeaderButton = passMeFurther => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={Icon}
    iconSize={25}
    color="white"
  />
);

export default class FilterNotificationsMenu extends Component {
  logout() {
    const { logout } = this.props;
    Alert.alert("Logout", "Are you sure you want to logout?", [
      {
        text: "Cancel",
        style: "cancel"
      },
      {
        text: "Accept",
        onPress: () => {
          logout();
        }
      }
    ]);
  }

  render() {
    const { updateFilterNotificationsFunction } = this.props;
    return (
      <HeaderButtons
        OverflowIcon={<Icon name="md-funnel" size={23} color="white" />}
        HeaderButtonComponent={IoniconsHeaderButton}
      >
        <Item
          title="Logout"
          iconName="md-log-out"
          onPress={() => this.logout()}
        />
        <Item
          title="Read"
          onPress={() => updateFilterNotificationsFunction(n => n.read)}
          show="never"
        />
        <Item
          title="Unread"
          onPress={() => updateFilterNotificationsFunction(n => !n.read)}
          show="never"
        />
        <Item
          title="All"
          onPress={() => updateFilterNotificationsFunction(() => true)}
          show="never"
        />
      </HeaderButtons>
    );
  }
}

FilterNotificationsMenu.propTypes = {
  logout: PropTypes.func.isRequired,
  updateFilterNotificationsFunction: PropTypes.func.isRequired
};
