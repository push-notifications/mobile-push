import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import api from "redux-cached-api-middleware";
import {
  findAllNotifications,
  NOTIFICATIONS_KEY
} from "../../notifications/actions/FindAllNotifications";
import { findAllServices } from "../../services/actions/FindAllServices";
import SearchPage from "../pages/SearchPage";
import { updateNotification } from "../../notifications/actions/UpdateNotification";

const mapStatetoProps = state => {
  return {
    notifications: state.notifications.notificationsList.notifications,
    services: state.services.servicesList.services
  };
};

const mapDispatchToProps = dispatch => {
  return {
    findAllNotifications: () => dispatch(findAllNotifications),
    ...bindActionCreators({ updateNotification, findAllServices }, dispatch),
    clearCache: () =>
      dispatch(
        api.actions.clearCache({
          cacheKey: NOTIFICATIONS_KEY
        })
      )
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(SearchPage);
